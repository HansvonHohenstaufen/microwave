## See LICENSE file for copyright and license details.
##
## MICROWAVE: MICROSTRIP
##
## -- Function File: Z0 = microstip_w2z (W, H, EE)
##
##	Get impedance from strip width.
##
##	*Inputs*
##	W
##		Width of the strip.
##	H
##		Thickness of substrate.
##	EE
##		Effective dielectric constant
##
##	*Output*
##	Z0
##		Characteristic impedance.

function [Z0] = microstrip_w2z(W, H, EE)
	if (W/H) < 1
		Z0 = (60/sqrt(EE)) * log((8*H/W)+(W/(4*H)));
	else
		Z0 = 120*pi/(sqrt(EE)*((W/H)+1.393+0.667*log(W/H+1.444)));
	endif
endfunction
