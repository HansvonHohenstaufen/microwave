## See LICENSE file for copyright and license details.
##
## MICROWAVE: MICROSTRIP
##
## -- Function File: EE = microstip_er2ee (ER, H, W)
##
##	Get effective dielectric constant from strip width.
##
##	*Inputs*
##	W
##		Width of the strip.
##	H
##		Thickness of substrate.
##	ER
##		Relative permittivity.
##
##	*Output*
##	EE
##		Effective dielectric constant

function [EE] = microstrip_er2ee(ER, H, W)
	EE = ((ER+1)/2) + (((ER-1)/2)*(1/sqrt(1+12*(H/W))));
endfunction
