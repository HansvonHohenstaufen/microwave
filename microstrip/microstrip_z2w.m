## See LICENSE file for copyright and license details.
##
## MICROWAVE: MICROSTRIP
##
## -- Function File: W = microstip_z2w (Z0, H, EE)
##
##	Get strip width from impedance.
##
##	*Inputs*
##	Z0
##		Characteristic impedance.
##	H
##		Thickness of substrate.
##	EE
##		Effective dielectric constant
##
##	*Output*
##	W
##		Width of the strip.

function [W] = microstrip_z2w(Z0, H, EE)
	A = ((Z0/60)*sqrt((EE+1)/2)) + (((EE-1)/(EE+1))*(0.23+(0.11/EE)));
	WD = 8*(e.^A)./((e.^(2*A))-2);
	if WD>2
		B = 337*pi./(2*Z0*sqrt(EE));
		WD = (2/pi) * (B-1-log(2*B-1)+(((EE-1)/(2*EE))*(log(B-1)+0.39-(0.61/EE))));
	endif
	W = WD*H;
endfunction
