## See LICENSE file for copyright and license details.
##
## MICROWAVE: SMITH CHART
##
## -- Function File: L_LAMBDA = smith_lambda_real (Z0, ZL, ZI_R)
##
##	Search real impedance by length of transmission line.
##
##	*Inputs*
##	Z0
##		Characteristic impedance.
##	ZL
##		Load impedance.
##	ZI_R
##		Real impedance to search.
##
##	*Output*
##	L_LAMBDA
##		Length of transmission line in lambda units.

function [L_LAMBDA] = smith_lambda_real(Z0, ZL, ZI_R)
	x = 0:1e-5:0.5;
	zt = smith_move(Z0, ZL, x);
	d = abs(real(ZI_R)-real(zt));
	[m, im] = min(d);
	L_LAMBDA = x(im);
endfunction
