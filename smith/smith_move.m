## See LICENSE file for copyright and license details.
##
## MICROWAVE: SMITH CHART
##
## -- Function File: ZI = smith_move (Z0, ZL, L_LAMBDA)
##
##	Get impedance by length of transmission line.
##
##	*Inputs*
##	Z0
##		Characteristic impedance.
##	ZL
##		Load impedance.
##	L_LAMBDA
##		Length of transmission line in lambda units.
##
##	*Output*
##	RHO
##		reflection coefficient.

function [ZI] = smith_move(Z0, ZL, L_LAMBDA)
	ZI	= Z0*(ZL+(j*Z0*tan(2*pi*L_LAMBDA)))./(Z0+(j*ZL*tan(2*pi*L_LAMBDA)));
endfunction
