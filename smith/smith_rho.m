## See LICENSE file for copyright and license details.
##
## MICROWAVE: SMITH CHART
##
## -- Function File: RHO = smith_rho (Z0, ZL)
##
##	Get reflection coefficient.
##
##	*Inputs*
##	Z0
##		Characteristic impedance.
##	ZL
##		Load impedance.
##
##	*Output*
##	RHO
##		reflection coefficient.

function [RHO] = smith_rho(Z0, ZL)
	RHO = (ZL-Z0)/(ZL+Z0);
endfunction
